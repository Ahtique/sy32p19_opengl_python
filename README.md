# SY32P19_OpenGL_python

## Overview

Translation from C to Python of my image synthesis course which make heavy use of OpenGL. Most of the given resources are in C, which I find less pleasant to use than Python.

## Requirements

PyOpenGL, PyOpenGL accelerate, freeglut