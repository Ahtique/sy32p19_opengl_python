//***************************************************************************//
// Fichier C                 TD Machine eclairage d'une sphere SY32          //
//                                   Universite de Technologie de Compiegne //
//                                                                           //
//  Interface OpenGL (GLUT, GLU, GL)                                         //
//  Librairies DLL : glut.dll                                                //
//                   glut32.dll                                              //
//                   opengl.dll										         //
//																		     //
//   facetisation d'une sphere : subdivision recursive                       //
//    Modes d'affichage :                                                   //
//			mode = 0	Filaire												//
// 			mode = 1    Flat												//
//			mode = 2	Gouraud sans lissage								//
//			mode = 3	Gouraud avec lissage								 //
//***************************************************************************//

//******************//
//Fichiers inclus : //
//******************//



#include <windows.h>
#include <math.h>
#include <stdio.h>

#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glaux.h>
#include <gl/glut.h>

#include <stdlib.h>

GLdouble MatObjet[16]; /* Matrice de transformation liee a  la sphere */
typedef double point[4];  /* point ou vecteur :coord homogenes */



point tetra[4] = {	{0., 0., 1.,1.},
                    {0., .942809, -0.333333,1.},
                    {-.816497,-.471405,-.3333333,1.},
                    {0.816497,-.471405,-0.333333,1.}
                 };

int mode = 0;
int n;



/*****************************************************************************/
void def_sources(void)
{
	/* definition d'une source GL_LIGHT0  */
  GLfloat props[4];    /* tableau des proprietes de la source */
  //GLenum source;

    {

	/* A completer   */
      props[ 0] = /* ... */;
      props[ 1] = /* ... */;
      props[ 2] = /* ... */;
      props[ 3] = /* ... */;
      glLightfv(GL_LIGHT0,GL_AMBIENT,props);


      props[ 0] = /* ... */;
      props[ 1] = /* ... */;
      props[ 2] = /* ... */;
      props[ 3] = /* ... */;
      glLightfv(GL_LIGHT0,GL_DIFFUSE,props);
      glLightfv(GL_LIGHT0,GL_SPECULAR,props);

      props[ 0] = /* ... */;
      props[ 1] = /* ... */;
      props[ 2] = /* ... */;
      props[ 3] = /* ... */;
      glLightfv(GL_LIGHT0,GL_POSITION,props);

      glLightf(GL_LIGHT0,GL_SPOT_CUTOFF,/* ... */);
      glLightf(GL_LIGHT0,GL_SPOT_EXPONENT,/*... */);

      props[ 0] = /* ... */;
      props[ 1] = /* ... */;
      props[ 2] = /* ... */;
      props[ 3] = /* ... */;
      glLightfv(GL_LIGHT0,GL_SPOT_DIRECTION,props);

      glEnable(GL_LIGHT0);
    }
}

/*****************************************************************************/
void def_modele(void)
{
  GLfloat props[4];
	/* A completer   */
  props[ 0] = /* ... */;
  props[ 1] = /* ... */;
  props[ 2] = /* ... */;
  props[ 3] = /* ... */;
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT,props);

  glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);

}

/*****************************************************************************/
void def_matiere(void)
{
   GLfloat props[4];

	/* A completer   */

      props[ 0] = /* ... */;
      props[ 1] = /* ... */;
      props[ 2] = /* ... */;
      props[ 3] = /* ... */;
      glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, props);

      props[ 0] = /* ... */;
      props[ 1] = /* ... */;
      props[ 2] = /* ... */;
      props[ 3] = /* ... */;
      glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, props);

      props[ 0] = /* ... */;
      props[ 1] = /* ... */;
      props[ 2] = /* ... */;
      props[ 3] = /* ... */;
      glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, props);

      glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, /* ... */);

      props[ 0] = /* ... */;
      props[ 1] = /* ... */;
      props[ 2] = /* ... */;
      props[ 3] = /* ... */;
      glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, props);

}


void dessine_triangle(void)
{
    /* A FAIRE */


}


void dessine_scene(void)
{

  dessine_repere(); /* A FAIRE  */
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  //glMultMatrixd(MatObjet);  a rajouter si animation et modification de la matrice
  tetrahedre(n); /* A FAIRE  */
  glPopMatrix();
}



void touche_clavier(unsigned char key, int x, int y)
{


  switch (key) {
  case '0' :
	  mode =0;
	  break;
  case '1' :
	  mode =1;
	  break;
  case '2' :
	  mode =2;
	  break;
  case '3' :
	  mode =3;
	  break;
  case 27:
    exit(0);
  }
    if (mode == 0) {
        glDisable(GL_LIGHTING);
    }
    else {
        glEnable(GL_LIGHTING);

        if (mode == 1)  glShadeModel(GL_FLAT);
        if ((mode == 2) || (mode == 3))    glShadeModel(GL_SMOOTH);
    }
    glutPostRedisplay();
}

/**************************************************************************/
/*     Fonctions d'affichage et de gestion des fenetres par glut          */
/**************************************************************************/
/**************************************************************************/
void initFunction(void)
{
    /* couleur de fond: noir */
    glClearColor(0.1, 0.1, 0.1, 0.0);
    /* couleur de fond: blanc  */
    //glClearColor(1.0, 1.0, 1.0, 0.0);

    /* initialisation de MatObjet*/
    glMatrixMode(GL_MODELVIEW),
    glLoadIdentity();
    //glGetDoublev(GL_MODELVIEW_MATRIX, MatObjet); /*recupere la matrice courante*/

}


/*************************************************************************/
/* lorsque la taille de la fenetre est modifiee */
void reshapeFunction(int width, int height)
{
    /* transformation de cadrage */
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    /* chargement de la matrice de projection*/
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    /* parametres de la perspective */
    gluPerspective(70,width/height,0.1,100 ) ;

    /* chargement de la matrice de transformations :scene - observateur */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(3.,1.,1., 0,0,0, 0,0,1);

}

/****************************************************************************/
/* rafraichissement de l'affichage */
void displayFunction(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    dessine_scene();
    glutSwapBuffers();
}


/****************************************************************************/
/*****************************************************************************/
/*     main                                                					*/
/*****************************************************************************/

int main(int argc, char **argv)
{

	/*A FAIRE saisie du nombre de subdivisions pour la fonction tetrahedre() */

	/* initialisation de GLUT */
	glutInit(&amp;argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(460,460);
	/* creation de la fenetre GLUT */
	glutCreateWindow("TD SY32");

	/* initialisations diverses*/
	initFunction();

	/* lorsque la taille de la fenetre est modifiee */
	glutReshapeFunc(reshapeFunction);
	/* rafraichissement de l'affichage */
	glutDisplayFunc(displayFunction);
	/* gestion des touches clavier*/
	/*glutSpecialFunc(touche_special_appuye);*/
	glutKeyboardFunc(touche_clavier);
    /* gestion de la souris */
    //glutMouseFunc(click_souris);
    //glutMotionFunc(click_dep_souris);

    /* activation de l'elimination des faces cachees */
	glEnable(GL_DEPTH_TEST);


	/* Definit les modes d'eclairage en fonction du mode choisi */
    glEnable(GL_LIGHTING);
    if (mode == 0) glColor3f(.8,.2,.1.);
    else {
        glEnable(GL_LIGHTING);
        if (mode == 1)  glShadeModel(GL_FLAT);
        if ((mode == 2) || (mode == 3))    glShadeModel(GL_SMOOTH);
    }

    /* definition des options d'eclairage propres � la scene  */
	def_modele();
	def_sources();
	def_matiere();
	/* demarrage de la boucle principale */
	glutMainLoop();

	return 0;
}



���������������
