from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys
import numpy as np
import math

name = b'snowman'
mode = 1


def afficheTriangle(A, B, C):
    glBegin(GL_POLYGON)
    # glBegin(GL_LINE_LOOP)
    # print ("A={}, B={}, C={} ".format(A,B,C))

    # calcul de la normale
    glNormal3dv(A)  # ici on a de la chance comme la sphère est centrée en 0,0,0 la normal est le vecteur _OA
    # affichage du vertex
    glVertex3f(A[0], A[1], A[2])

    # etc
    glNormal3dv(B)
    glVertex3f(B[0], B[1], B[2])
    glNormal3dv(C)
    glVertex3f(C[0], C[1], C[2])
    glEnd()


def facettisationSphere(n, A, B, C):
    if (n <= 0): return afficheTriangle(A, B, C)
    MAB = np.zeros(3)
    MAC = np.zeros(3)
    MBC = np.zeros(3)
    for j in range(3):
        MAB[j] = (A[j] + B[j])
        MAC[j] = (A[j] + C[j])
        MBC[j] = (B[j] + C[j])
    MAB /= normalise(MAB)
    MAC /= normalise(MAC)
    MBC /= normalise(MBC)

    # print ("MAB={}, MAC={}, MBC={} ".format(np.sum(A),B,C))

    facettisationSphere(n - 1, A, MAB, MAC)
    facettisationSphere(n - 1, MAB, B, MBC)
    facettisationSphere(n - 1, MAC, MBC, C)
    facettisationSphere(n - 1, MAB, MBC, MAC)


def normalise(M):
    norme = 0
    for j in range(3):
        norme = norme + M[j] ** 2
    return math.sqrt(norme)


def initFacetisation(n):
    r = 2.0 * math.sqrt(2.0) / 3.0
    d = 1.0 / 3.0
    A = [.0, .0, 1.0]
    B = [.0, r, -d]
    C = [-r * math.sqrt(3.0) / 2.0, -r / 2.0, -d]
    D = [r * math.sqrt(3.0) / 2.0, -r / 2.0, -d]

    facettisationSphere(n, A, D, C)
    facettisationSphere(n, A, C, B)
    facettisationSphere(n, A, B, D)
    facettisationSphere(n, B, C, D)


def def_sources():
    # lightZeroPosition = [10., 4., 10., 1.]
    # lightZeroColor = [0.8, 1.0, 0.8, 1.0]  # green tinged

    # éclairage ambiant doit rester bas
    props = [0.7, 0.7, 0.7, 1.0]
    glLightfv(GL_LIGHT0, GL_AMBIENT, props)

    # Source0 val élevée pour éclairé
    props = [1, 1, 1, 1]
    glLightfv(GL_LIGHT0, GL_DIFFUSE, props)
    glLightfv(GL_LIGHT0, GL_SPECULAR, props)

    # Position de la source 0
    props = [5, 0.0, 4.0, 1]
    glLightfv(GL_LIGHT0, GL_POSITION, props)
    glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 180)  # angle d'ouverture du spot 180°
    glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 10)  # exposant de la reflection ?

    # orientation du spot vers le centre du repère
    props = [-5.0, 0.0, -4.0, 1.0]
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, props)
    glEnable(GL_LIGHT0)


'''
    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.1)
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.05)
'''


def def_matiere():
    # matière de l'objet RGB

    props = [0.4, 0.2, 0.1, 1]
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, props)

    props = [0.8, 0.2, 0.1, 1]
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, props)

    props = [0.4, 0.5, 0.5, 1]
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, props)

    # si shiness élebé l'objet vas être tout blanc
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 2)

    # pas d'émission de lumière depuis l'objet
    props = [.0, .0, .0, 1.]
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, props)


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(400, 400)
    glutCreateWindow(name)

    # innit
    glClearColor(0.1, 0.1, 0.1, 0.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    # enable
    glEnable(GL_CULL_FACE)
    glEnable(GL_DEPTH_TEST)

    glutDisplayFunc(display)

    # lighting
    glEnable(GL_LIGHTING)
    if mode == 0:
        glColor3f(.8, .2, .1)
    else:
        glEnable(GL_LIGHTING)
        if mode == 1:  glShadeModel(GL_FLAT)
        if (mode == 2) or (mode == 3):    glShadeModel(GL_SMOOTH)

    def_sources()
    def_matiere()

    # def modele
    props = [.01, .01, .01, 1]
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, props)
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE)
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE)

    glMatrixMode(GL_PROJECTION)
    gluPerspective(40., 1., 1., 40.)
    glMatrixMode(GL_MODELVIEW)
    gluLookAt(-5, -5, -5,
              0, 0, 0,
              0, 1, 0)
    glPushMatrix()
    glutMainLoop()
    return


def display():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    def_sources()

    glPushMatrix()
    glBegin(GL_LINES)
    glVertex2i(0, 0)
    glVertex2i(0, 1)
    glVertex2i(0, 0)
    glVertex2i(1, 0)
    glVertex2i(0, 0)
    glVertex3i(0, 0, 1)
    glEnd()

    initFacetisation(4)
    glPopMatrix()

    glutSwapBuffers()
    return


'''    
    #dessine scene
    glPushMatrix()
    white = [1.0, 1, 1, 1.]
    red = [1.0, 0.1, 0.1]
    glMaterialfv(GL_FRONT, GL_DIFFUSE, white)
    # body
    glutSolidSphere(2, 100, 100)
    # head
    glTranslate(0, 2.7, 0)
    glutSolidSphere(1, 100, 100)
    # hat
    glPushMatrix()
    glTranslate(0, 1, 0)
    glRotate(-90, 1, 0, 0)
    glMaterialfv(GL_FRONT, GL_DIFFUSE, red)
    glutSolidCone(1, 1, 100, 100)
    glPopMatrix()
    # nose
    glTranslate(0, 0, 1)
    glutSolidCone(.2, 1, 10, 10)
    glutSwapBuffers()
    return
'''

main()
