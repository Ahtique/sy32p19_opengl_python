from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys

name = b'snowman'


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(400, 400)
    glutCreateWindow(name)

    glClearColor(0., 0., 0., 1.)
    glShadeModel(GL_SMOOTH)
    glEnable(GL_CULL_FACE)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_LIGHTING)
    lightZeroPosition = [10., 4., 10., 1.]
    lightZeroColor = [0.8, 1.0, 0.8, 1.0]  # green tinged
    glLightfv(GL_LIGHT0, GL_POSITION, lightZeroPosition)
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightZeroColor)
    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.1)
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.05)
    glEnable(GL_LIGHT0)
    glutDisplayFunc(display)
    glMatrixMode(GL_PROJECTION)
    gluPerspective(40., 1., 1., 40.)
    glMatrixMode(GL_MODELVIEW)
    gluLookAt(5, 10, 15,
              0, 0, 0,
              0, 1, 0)
    glPushMatrix()
    glutMainLoop()
    return


def display():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glPushMatrix()
    white = [1.0, 1, 1, 1.]
    red = [1.0, 0.1, 0.1]
    glMaterialfv(GL_FRONT, GL_DIFFUSE, white)
    # body
    glutSolidSphere(2, 100, 100)
    # head
    glTranslate(0, 2.7, 0)
    glutSolidSphere(1, 100, 100)
    # hat
    glPushMatrix()
    glTranslate(0, 1, 0)
    glRotate(-90, 1, 0, 0)
    glMaterialfv(GL_FRONT, GL_DIFFUSE, red)
    glutSolidCone(1, 1, 100, 100)
    glPopMatrix()
    # nose
    glTranslate(0, 0, 1)
    glutSolidCone(.2, 1, 10, 10)
    glutSwapBuffers()
    return


main()
